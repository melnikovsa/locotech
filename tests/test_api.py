
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status

from locotech.management.commands import fill_db


class APITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        fill_db.Command().handle()

    def test_index_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_calc_proceeds(self):
        response = self.client.get('/api/calc_proceeds/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.get('/api/calc_proceeds/?year_from=0&year_to=0&filial=0&series=0')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertTrue('data' in response.json() and isinstance(response.json()['data'], list) and
                        len(response.json()['data']))

        response = self.client.get('/api/calc_proceeds/?year_from=2018&year_to=0&filial=0&series=0')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('data' in response.json() and isinstance(response.json()['data'], list) and
                        len(response.json()['data']))

        response = self.client.get('/api/calc_proceeds/?year_from=2018&year_to=2020&filial=0&series=0')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('data' in response.json() and isinstance(response.json()['data'], list) and
                        len(response.json()['data']))
