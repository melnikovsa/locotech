

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from locotech.models import Filials, Series


class Dashboard(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'index.html'

    def get(self, request):
        filials = Filials.objects.all().order_by('name')
        series = Series.objects.all().order_by('name')
        return Response({'filials': filials, 'series': series})
