
import datetime
import xlrd
from django.core.management.base import BaseCommand, CommandError
from locotech.models import Mileages, Filials, Series


class Command(BaseCommand):
    def __init__(self):
        super().__init__()
        self. excel_path = 'install/test_LT.xlsx'

    help = 'Command for fill DB data'

    @staticmethod
    def clear_db_data():
        Mileages.objects.all().delete()
        Filials.objects.all().delete()
        Series.objects.all().delete()

    @staticmethod
    def read_filials(sheet):
        for row_num in range(0, sheet.nrows):
            row = sheet.row_values(row_num)
            Filials.objects.create(name=row[0])

    @staticmethod
    def read_series(sheet):
        for row_num in range(1, sheet.nrows):
            row = sheet.row_values(row_num)
            Series.objects.create(name=row[0], rate=float(str(row[1]).replace(',', '.')))

    @staticmethod
    def read_mileage(sheet):
        row_header = sheet.row_values(0)

        for row_num in range(1, sheet.nrows):
            row = sheet.row_values(row_num)
            filial = Filials.objects.get(name=row[0])
            series = Series.objects.get(name=row[1])

            for col_num in range(2, len(row)):
                Mileages.objects.create(series=series, filial=filial, year=row_header[col_num], mileage=row[col_num])

    def read_excel_data(self):
        rb = xlrd.open_workbook(self.excel_path)

        self.read_filials(rb.sheet_by_index(3))
        self.read_series(rb.sheet_by_index(2))
        self.read_mileage(rb.sheet_by_index(1))

    def handle(self, *args, **options):
        try:
            self.clear_db_data()
            self.read_excel_data()
        except Exception as err:
            raise CommandError('{}: Error fill DB data: {}'.format(str(datetime.datetime.now()), str(err)))

        self.stdout.write('{}: Successfully fill DB data'.format(str(datetime.datetime.now())))
