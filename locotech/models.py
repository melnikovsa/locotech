
from django.db import models


class Series(models.Model):
    name = models.CharField(max_length=100, unique=True)
    rate = models.FloatField()


class Filials(models.Model):
    name = models.CharField(max_length=100, unique=True)


class Mileages(models.Model):
    series = models.ForeignKey('Series', on_delete=models.CASCADE)
    filial = models.ForeignKey('Filials', on_delete=models.CASCADE)
    year = models.SmallIntegerField()
    mileage = models.BigIntegerField()
