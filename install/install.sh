#!/usr/bin/env bash
LOCOTECH_DIRECTORY=/vagrant

sudo apt-get update
# sudo apt-get upgrade -y
sudo apt-get install nginx -y
sudo cp install/locotech /etc/nginx/sites-available
sudo ln -s /etc/nginx/sites-available/locotech /etc/nginx/sites-enabled/locotech
sudo service nginx restart

sudo apt-get install mariadb-server -y
sudo apt-get install libmysqlclient-dev -y
mysql < install/db.sql

sudo apt-get install supervisor -y
sudo cp install/locotech.conf /etc/supervisor/conf.d

sudo apt-get install python3-pip -y
sudo pip3 install -r requirements.txt
sudo ln -s ${LOCOTECH_DIRECTORY} /usr/local/lib/python3.4/dist-packages/locotech

# make migrations
sudo python3 manage.py makemigrations
sudo python3 manage.py makemigrations locotech
sudo python3 manage.py migrate

# fill DB data
sudo python3 manage.py fill_db

sudo service supervisor restart
