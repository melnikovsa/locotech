create database locotech;

use locotech;

GRANT ALL PRIVILEGES ON locotech.* TO 'locotech'@'localhost' IDENTIFIED BY 'vbhrxFEv';

-- grant access for test database
GRANT ALL PRIVILEGES ON test_locotech.* TO 'locotech'@'localhost' IDENTIFIED BY 'vbhrxFEv';