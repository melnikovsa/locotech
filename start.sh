#!/bin/bash
set -e
LOGFILE=/var/log/locotech.log
LOGDIR=$(dirname ${LOGFILE})
SITE=/usr/local/lib/python3.4/dist-packages/locotech/
export DJANGO_SETTINGS_MODULE=locotech.settings
ENV=`hostname`
# Test on environment
# user/group to run as
USER=www-data
GROUP=www-data
NUM_WORKERS=3

export PYTHONPATH=${SITE}:${PYTHONPATH};

PORT=8005
cd ${SITE}
test -d ${LOGDIR} || mkdir -p ${LOGDIR}

export DJANGO_SETTINGS_MODULE=locotech.settings

# Start Django over gunicorn workers
exec gunicorn --env DJANGO_SETTINGS_MODULE=locotech.settings locotech.wsgi:application -b 127.0.0.1:${PORT} -w ${NUM_WORKERS} \
  --user=${USER} --group=${GROUP} --log-level=debug   --log-file=${LOGFILE} 2>>${LOGFILE} --timeout=3600