from django.db.models import F, FloatField, Sum
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from locotech.models import Mileages


class CalcProceeds(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request):
        try:
            year_from = request.query_params['year_from']
            year_to = request.query_params['year_to']
            filial_id = request.query_params['filial']
            series_id = request.query_params['series']

            s = Mileages.objects
            if year_from and str(year_from).isdigit() and int(year_from):
                s = s.filter(year__gte=year_from)

            if year_to and str(year_to).isdigit() and int(year_to):
                s = s.filter(year__lte=year_to)

            if filial_id and str(filial_id).isdigit() and int(filial_id):
                s = s.filter(filial_id=filial_id)

            if series_id and str(series_id).isdigit() and int(series_id):
                s = s.filter(series_id=series_id)

            s = s.values('year').annotate(proceeds=Sum(F('mileage') * F('series__rate'), output_field=FloatField())).\
                order_by('year')

            return Response({
                'data': list(s)
            })

        except Exception:
            return Response(status=400)
